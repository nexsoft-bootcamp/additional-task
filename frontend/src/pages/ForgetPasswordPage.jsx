import {useNavigate} from "react-router-dom";
import {useState} from "react";

const ForgetPasswordPage = () => {

    const [formData, setFormData] = useState({username: ''});

    const navigate = useNavigate()
    const handleChange = (event) => {
        const {name, value} = event.target;
        setFormData({...formData, [name]: value});
    };

    const onSubmit = (e) => {
        e.preventDefault()
        fetch('http://localhost:8080/api/forget-password', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
        })
            .then(response => {
                if (!response.ok) {
                    alert('Forget Password Error');
                }
                navigate("/")
            })
            .catch(error => {
                console.error('Error:', error);
                alert("Server Error: " + error.message);
            });
    }

    return (
        <section className="bg-gray-50">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <div className="w-screen bg-white rounded-lg shadow md:mt-0 sm:max-w-md xl:p-0">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
                            Reset Your Password
                        </h1>
                        <form className="space-y-4 md:space-y-6" onSubmit={onSubmit}>
                            <label className="form-control w-full">
                                <div className="label">
                                    <span className="label-text">Username</span>
                                </div>
                                <input value={formData.username}
                                       onChange={handleChange} name={`username`} type="text" placeholder="e.g johndoe"
                                       className="input input-bordered w-full" required={true}/>
                            </label>
                            <button type={`submit`} className="btn btn-primary btn-block">Forget Password</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ForgetPasswordPage;