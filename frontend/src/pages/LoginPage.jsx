import {useEffect, useState} from "react";
import {Link, useNavigate} from "react-router-dom";

const LoginPage = () => {

    const [formData, setFormData] = useState({username: '', password: ''});
    const handleChange = (event) => {
        const {name, value} = event.target;
        setFormData({...formData, [name]: value});
    };

    const navigate = useNavigate()

    const onSubmit = (e) => {
        e.preventDefault()
        fetch('http://localhost:8080/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
        })
            .then(response => {
                if (!response.ok) {
                    alert('Failed to login');
                }
                return response.text()
            }).then(res => {
            localStorage.setItem('token', res);
            navigate("/home")
        })
            .catch(error => {
                console.error('Error:', error);
                alert("Failed to login: " + error.message);
            });
    }

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            navigate("/home")
        }
    }, [navigate]);

    return (
        <section className="bg-gray-50">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <div className="w-screen bg-white rounded-lg shadow md:mt-0 sm:max-w-2xl xl:p-0">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
                            Sign in to your account
                        </h1>
                        <form className="flex w-full" onSubmit={onSubmit}>
                            <div className={`space-y-4 md:space-y-6 flex-1`}>
                                <label className="form-control w-full">
                                    <div className="label">
                                        <span className="label-text">Username</span>
                                    </div>
                                    <input value={formData.username}
                                           onChange={handleChange} name={`username`} type="text"
                                           placeholder="e.g johndoe"
                                           className="input input-bordered w-full" required={true}/>
                                </label>
                                <label className="form-control w-full">
                                    <div className="label">
                                        <span className="label-text">Password</span>
                                    </div>
                                    <input value={formData.password}
                                           onChange={handleChange} name={`password`} type="password"
                                           placeholder="******"
                                           className="input input-bordered w-full" required={true}/>
                                </label>
                                <div>
                                    <Link to={`/forget-password`} className="text-sm font-medium text-primary-600 hover:underline">Forgot
                                        password?</Link>
                                </div>
                                <button type={`submit`} className="btn btn-primary btn-block">Sign in</button>
                                <p className="text-sm font-light text-gray-500">
                                    Don’t have an account yet? <Link to={`/register`}
                                                                     className="font-medium text-primary-600 hover:underline">Sign
                                    up</Link>
                                </p>
                            </div>
                            <div className="divider lg:divider-horizontal"></div>
                            <div className={`flex items-center flex-1`}>
                                <button onClick={() => alert("not ready yet")} className="btn btn-block">Google</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default LoginPage;