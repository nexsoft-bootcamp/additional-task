import {useEffect, useState} from "react";
import {Link, redirect, useNavigate} from "react-router-dom";

const RegisterPage = () => {

    const [formData, setFormData] = useState({username: '', name: '', password: ''});

    const handleChange = (event) => {
        const {name, value} = event.target;
        setFormData({...formData, [name]: value});
    };

    const navigate = useNavigate()

    const onSubmit = (e) => {
        e.preventDefault()
        fetch('http://localhost:8080/api/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
        })
            .then(response => {
                if (!response.ok) {
                    alert('Sign Up Error');
                }
                navigate("/")
            })
            .catch(error => {
                console.error('Error:', error);
                alert("Server Error: " + error.message);
            });
    }

    return (
        <section className="bg-gray-50">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <div className="w-screen bg-white rounded-lg shadow md:mt-0 sm:max-w-xl xl:p-0">
                    <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
                            Sign Up New Account
                        </h1>
                        <form className="space-y-4 md:space-y-6" onSubmit={onSubmit}>
                            <label className="form-control w-full">
                                <div className="label">
                                    <span className="label-text">Username</span>
                                </div>
                                <input value={formData.username}
                                       onChange={handleChange} name={`username`} type="text" placeholder="e.g johndoe"
                                       className="input input-bordered w-full" required={true}/>
                            </label>
                            <label className="form-control w-full">
                                <div className="label">
                                    <span className="label-text">Name</span>
                                </div>
                                <input value={formData.name}
                                       onChange={handleChange} name={`name`} type="text" placeholder="e.g John Doe"
                                       className="input input-bordered w-full" required={true}/>
                            </label>
                            <label className="form-control w-full">
                                <div className="label">
                                    <span className="label-text">Password</span>
                                </div>
                                <input value={formData.password}
                                       onChange={handleChange} name={`password`} type="password" placeholder="******"
                                       className="input input-bordered w-full" required={true}/>
                            </label>
                            <button type={`submit`} className="btn btn-primary btn-block">Sign Up</button>
                            <p className="text-sm font-light text-gray-500">
                                Already have a account? <Link to={`/`}
                                                              className="font-medium text-primary-600 hover:underline">Sign
                                in</Link>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default RegisterPage;