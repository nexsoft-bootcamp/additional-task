import {useEffect} from 'react';
import {useNavigate} from "react-router-dom";

const Home = () => {

    const navigate = useNavigate()

    const handleLogout = () => {
        localStorage.removeItem("token")
        navigate("/")
    }

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            alert("You must login")
            navigate("/")
        }
    }, [navigate]);

    return (
        <div className={`container mx-auto p-4 h-screen flex flex-col gap-4 items-center justify-center`}>
            <div className={`font-semibold mb-4`}>
                Welcome to ea
            </div>
            <div onClick={handleLogout} className={`btn btn-sm btn-neutral`}>Logout</div>
        </div>
    );
};

export default Home;