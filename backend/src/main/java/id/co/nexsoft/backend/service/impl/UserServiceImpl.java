package id.co.nexsoft.backend.service.impl;

import id.co.nexsoft.backend.utils.JwtUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import id.co.nexsoft.backend.entity.User;
import id.co.nexsoft.backend.repository.UserRepository;
import id.co.nexsoft.backend.service.UserService;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public String createUser(User user) {
        User newUser = new User();
        User userFound = userRepository.findByUsername(user.getUsername());
        if (userFound == null) {
            newUser.setName(user.getName());
            newUser.setUsername(user.getUsername());
            newUser.setPassword(this.passwordEncoder.encode(user.getPassword()));
            userRepository.save(newUser);
            return newUser.getName();
        } else {
            return "Username Already Exist";
        }
    }

    @Override
    public String loginMessage(User user) {
        User foundUser = userRepository.findByUsername(user.getUsername());
        if (foundUser != null) {
            String password = user.getPassword();
            String encodedPassword = foundUser.getPassword();
            Boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
            System.out.println(isPwdRight);
            if (isPwdRight) {
                String jwt;
                jwt = createJWT(foundUser.getUsername(), foundUser.getId());
                return jwt;
            } else {
                return "Password not match";
            }
        }
        return "Username not Found";
    }

    private String createJWT(String username, Long userId) {
        String secretString = "2c70e12b7a0646f92279f427c7b38e7334d8e5389cff167a1dc30e73f826b683";

        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", userId);

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));

        String jwt;
        jwt = Jwts.builder()
                .claims(claims)
                .subject(username)
                .issuedAt(new Date())
                .expiration(new Date(System.currentTimeMillis() + 86400000))
                .signWith(key)
                .compact();

        return jwt;
    }

    private String getUserNameFromJwtToken(String token) {
        String secretString = "2c70e12b7a0646f92279f427c7b38e7334d8e5389cff167a1dc30e73f826b683";
        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretString));
        return Jwts.parser().verifyWith(key).build()
                .parseSignedClaims(token).getPayload().getSubject();
    }


}

