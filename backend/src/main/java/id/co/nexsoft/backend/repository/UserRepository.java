package id.co.nexsoft.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.backend.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByUsernameAndAndPassword(String username, String password);
}

