package id.co.nexsoft.backend.controller;

import id.co.nexsoft.backend.entity.User;
import id.co.nexsoft.backend.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ForgetPasswordController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PatchMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(@RequestBody User username) {
        User userFound = userRepository.findByUsername(username.getUsername());
        if (userFound == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }

        String resetToken = generateResetToken();
        userFound.setResetToken(resetToken);
        userRepository.save(userFound);

        return ResponseEntity.ok("Password reset instructions sent to your email");
    }

    @PostMapping("/reset-password")
    public ResponseEntity<String> resetPassword(@RequestBody ResetDto resetDto) {
        User user = userRepository.findByUsername(resetDto.getUsername());
        if (user == null || !resetDto.getToken().equals(user.getResetToken())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid token");
        }

        user.setPassword(this.passwordEncoder.encode(resetDto.getPassword()));
        user.setResetToken(null);
        userRepository.save(user);

        return ResponseEntity.ok("Password reset successfully");
    }

    private String generateResetToken() {
        return UUID.randomUUID().toString();
    }

}

@Data
class ResetDto {
    private String token;
    private String username;
    private String password;
}
